#!/bin/sh

if ! [ -x "$(command -v npm)" ]; then
  echo 'INFO: Installing ''npm'' using brew ' >&2

    if ! [ -x "$(command -v brew)" ]; then
        echo 'INFO: Installing brew ' >&2
        brew install npm
    fi
fi

npm init -y && \
npm install --save-dev webpack babel style-loader css-loader sass-loader node-sass && \
npm install --save react react-dom babel-core babel-loader babel-preset-es2015 babel-preset-react

npm run build
