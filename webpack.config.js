const path = require('path');

const APP_DIR = path.resolve(__dirname, 'src/');

module.exports = {
  entry: APP_DIR + '/index.jsx',
  resolve: {
    modules: [
      path.resolve('./node_modules'),
      path.resolve('./src')
    ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
        {
            test : /\.jsx?/,
            include : APP_DIR,
            loader : 'babel-loader'
        },
        {
            test:/\.(s*)css$/,
            use:['style-loader','css-loader', 'sass-loader']
        }
    ]
  }
};
